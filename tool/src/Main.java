import java.io.*;
import java.nio.file.Files;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
        String folderPath = "C:\\Users\\Trung Do\\Desktop\\NCKH\\macrodetectionsupporttool\\dataset\\C";
        File file = new File(folderPath);
        String content = "";
        if (file.isDirectory()) {
            for (File project : file.listFiles()) {
                System.out.println("Start calculate " + project.getAbsolutePath());
                int countMacro = checkProject(project);
                System.out.println("Project " + project.getAbsolutePath() + " count macro = " + countMacro);
                content += "Project " + project.getAbsolutePath() + " count macro = " + countMacro + "\n";
            }
        }
        try {
            File output = new File(folderPath + "/report.txt");
            if (!output.exists())
                output.createNewFile();
            writeContentToFile(content, output.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static int checkProject(File project) {
        int count = 0;
        if (!project.isDirectory()) {
            try {
                count += checkMacroForFile(project);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            for (File child : project.listFiles()) {
                count += checkProject(child);
            }
        }
        return count;
    }

    public static int checkMacroForFile(File file) throws IOException {
        int count = 0;
        if (file.getAbsolutePath().endsWith(".c") || file.getAbsolutePath().endsWith(".cpp")
                || file.getAbsolutePath().endsWith(".h") || file.getAbsolutePath().endsWith(".hpp")) {
            InputStream inputStream = Files.newInputStream(file.toPath());
            StringBuilder resultStringBuilder = new StringBuilder();
            try (BufferedReader br
                         = new BufferedReader(new InputStreamReader(inputStream))) {
                String line;
                while ((line = br.readLine()) != null) {
                    resultStringBuilder.append(line).append("\n");
                    if (line.contains("#define"))
                        count++;
                }
            }
        }
        return count;
    }

    public static String readFromInputStream(InputStream inputStream)
            throws IOException {
        int count = 0;
        StringBuilder resultStringBuilder = new StringBuilder();
        try (BufferedReader br
                     = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line).append("\n");
                if (line.contains("#define"))
                    count++;
            }
        }
        return resultStringBuilder.toString();
    }

    public static void writeContentToFile(String content, String path)
            throws IOException {

        BufferedWriter writer = new BufferedWriter(new FileWriter(path));
        writer.write(content);

        writer.close();
    }
}