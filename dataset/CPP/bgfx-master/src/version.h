/*
 * Copyright 2011-2023 Branimir Karadzic. All rights reserved.
 * License: https://github.com/bkaradzic/bgfx/blob/master/LICENSE
 */

/*
 *
 * AUTO GENERATED! DO NOT EDIT!
 *
 */

#define BGFX_REV_NUMBER 8615
#define BGFX_REV_SHA1   "67107e5511ee09896643d0850574ae1485fa44c9"
